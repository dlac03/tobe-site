import React, { Fragment } from 'react';
import { FormattedMessage } from 'react-intl';

export const EventList = ({ events }) => {
    return Object.keys(events).map(yearKey => {
        const year = events[yearKey];
        return (
            <div key={yearKey}>
                <h2>{yearKey}</h2>
                {
                    Object.keys(year).map(monthKey => {
                        const month = year[monthKey];
                        return (
                            <Fragment key={monthKey}>
                                <MonthListing monthKey={monthKey} />
                                <EventListing events={month.events} />
                            </Fragment>
                        )
                    })
                }
            </div>
        )
    })
}

const MonthListing = ({ monthKey }) => (
    <h3 key={monthKey}>
        <span className="icon fa-calendar-o"></span>
        <span> <FormattedMessage id={`months.${monthKey}`} /></span>
    </h3>
)

const EventListing = ({ events }) => (
    <ul className="event-list">
        {
            events.map(event => (
                <li key={event.name} className="list-item">{event.date}. {event.name} {event.venue ? `- ${event.venue}` : ''}</li>
            ))
        }
    </ul>
);