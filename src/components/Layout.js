import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import { StaticQuery, graphql } from 'gatsby'

import '../assets/scss/main.scss'

const Layout = ({ children, location, locale }) => {

  let content;

  if (location && location.pathname === '/') {
    content = (
      <div>
        {children}
      </div>
    )
  } else {
    content = (
      <div id="wrapper" className="page">
        <div>
          {children}
        </div>
      </div>
    )
  }

  return (
    <StaticQuery
      query={graphql`
        query SiteTitleQuery {
          site {
            siteMetadata {
              title
            }
          }
        }
      `}
      render={data => (
        <>
          <Helmet
            title={data.site.siteMetadata.title}
            meta={[
              { name: 'description', content: 'ToBe Art Productions Website' },    
              { name: 'og:site_name', content: 'ToBe Art Productions Website' },
              { name: 'og:title', content: 'ToBe Art Productions Website' },
              { name: 'og:description', content: 'ToBe Art Productions Website' },
              { name: 'og:locale', content: {locale} },
              { name: 'og:type', content: 'website' },
              { name: 'og:image', content: '/static/f164366559e8ee462f4f8a1a0326e720/6833b/bg-sm.webp' },
              { name: 'keywords', content: 'events, concerts, musicals' }, 
            ]} 
          >
            <html lang={locale} />
          </Helmet>
          {content}
        </>
      )}
    />
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
