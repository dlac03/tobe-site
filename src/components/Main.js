import PropTypes from 'prop-types'
import React from 'react'
import { About, Productions, Gallery, Events, Contact } from '../content'

class Main extends React.Component {
  render() {
    const close = (
      <div
        className="close"
        role="button"
        onClick={this.props.onCloseArticle}
      ></div>
    )

    const { articleTimeout, article } = this.props;

    return (
      <div
        ref={this.props.setWrapperRef}
        id="main"
        style={this.props.timeout ? { display: 'flex' } : { display: 'none' }}>
        <About articleTimeout={articleTimeout} article={article} close={close} />
        <Productions articleTimeout={articleTimeout} article={article} close={close} />
        <Gallery articleTimeout={articleTimeout} article={article} close={close} />
        <Events articleTimeout={articleTimeout} article={article} close={close} />
        <Contact articleTimeout={articleTimeout} article={article} close={close} onCloseArticle={this.props.onCloseArticle} />
      </div>
    )
  }
}

Main.propTypes = {
  route: PropTypes.object,
  article: PropTypes.string,
  articleTimeout: PropTypes.bool,
  onCloseArticle: PropTypes.func,
  timeout: PropTypes.bool,
  setWrapperRef: PropTypes.func.isRequired,
}

export default Main
