module.exports = {
    hu: {
        path: 'hu',
        locale: 'Hungarian',
        default: true
    },
    en: {
        path: 'en',
        locale: 'English'
    }
}