export class ArtEvent {
    constructor(date, name, venue) {
        this.date = date;
        this.name = name;
        this.venue = venue;
    }
}